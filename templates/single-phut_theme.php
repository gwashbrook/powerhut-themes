<?php
/*
 * Template for display in single Powerhut Theme custom post type
 *
 *
 *
 */



//* Enqueue scripts
add_action( 'wp_enqueue_scripts', 'phut_single_enqueue' );
function phut_single_enqueue() {
	wp_enqueue_script( 'single-theme', plugins_url() . '/powerhut-themes/assets/js/phut-single-theme.js' , array( 'jquery' ), '1.0', true );
}



add_action('genesis_before','theme_demo');
function theme_demo(){
?>


<div id="theme-demo">
		
		<div class="container">
			<a href="#" class="close-demo"><span class="icon-x"></span> Close Demo</a>
			
			<div class="full-screen">
				<iframe src="<?php echo get_field('demo_url') ?>"></iframe>
			</div>
			
		<!--
        	<div class="tablet-screen">
				<iframe src="<?php echo get_field('demo_url') ?>"></iframe>
			</div>
			
			<div class="mobile-screen">
				<iframe src="<?php echo get_field('demo_url') ?>"></iframe>
			</div>
			
			<div class="panel">
				<h3 class="small-title">Genesis Framework</h3>
				
					<div class="icons">
					<a href="#" class="icon-monitor active"></a>
					<a href="#" class="icon-tablet"></a>
					<a href="#" class="icon-mobile"></a>
				</div>
								
				<a href="#" class="btn-green-big small button white-hover">See Pricing and Buy Theme</a>
			</div>
          -->
		</div>
		
	</div>	
<?php
}



// Set Schema




// Remove the post info function from the entry header
remove_action ('genesis_meta','child_maybe_move_post_info' );
remove_action( 'genesis_entry_header', 'genesis_post_info', 12 );

// add_action ( 'genesis_entry_header', 'phut_theme_meta' );
function phut_theme_meta(){

	// echo get_the_term_list( get_the_ID(), 'phut_theme_tag', '<p class="entry-meta">Tags: <span class="entry-tags">', ' ', '</span></p>' );
	// echo get_the_term_list( get_the_ID(), 'phut_theme_category', '<p class="entry-meta"><span class="entry-categories">More themes by ', ' ', '</span></p>' );

	echo '<p class="entry-meta">Tagged: <span class="entry-tags">';
	echo strip_tags( get_the_term_list( get_the_ID(), 'phut_theme_tag', '', ', ', '' ) );
	echo '</span></p>';

	echo '<p class="entry-meta">Categories: <span class="entry-categories">';
	echo strip_tags( get_the_term_list( get_the_ID(), 'phut_theme_category', '', ', ', '' ) );
	echo '</span></p>';	



} // fn


add_action( 'genesis_entry_footer','theacf');



// Replace default loop with custom
remove_action( 'genesis_loop', 'genesis_do_loop' );
add_action( 'genesis_loop', 'phut_single_theme_loop' );

// The custom loop
function phut_single_theme_loop(){

	if ( have_posts() ) :

		do_action( 'genesis_before_while' );
		while ( have_posts() ) : the_post();

			do_action( 'genesis_before_entry' );
			
			printf( '<article %s>', genesis_attr( 'entry' ) );

			// if( has_post_thumbnail()) the_post_thumbnail('single-cover', array('itemprop'=>'image'));

			?>
          

	<div class="theme-body">
  

		<?php do_action( 'genesis_entry_header' ); ?>
        <?php do_action( 'genesis_before_entry_content' ); ?>
	        <div class="entry-content"><?php the_content() ?></div>
		<?php do_action( 'genesis_after_entry_content' );	?>
  	
    
    	<?php do_action( 'genesis_entry_footer' ); ?>

	</div><!--// theme-body -->


<?php
echo '</article>';

			do_action( 'genesis_after_entry' );

		endwhile; //* end of one post
		do_action( 'genesis_after_endwhile' );

	else : //* if no posts exist
		do_action( 'genesis_loop_else' );
	endif; //* end loop

} //fn





function theacf() {
	
	if( get_field('demo_url') ) {
		echo '<a class="button button-lg full-demo" href="' . get_field('demo_url') . '" target="_blank" rel="nofollow">Theme demo</a>';
	}
	
	if( get_field('purchase_url') ) {
		
		// Capability 'edit_pages' = Super admin, Administrator, Editor
		if( has_term( 'powerhut', 'phut_theme_category' ) || current_user_can( 'edit_pages' ) ){
			echo '<a class="button button-primary button-lg" title="Buy ' . esc_attr( get_the_title() ) . '" target="_blank" href="' . trailingslashit( get_field('purchase_url') ) . '">Buy now</a>';
		} else {
			echo '<a class="button button-primary button-lg external" title="Buy ' . esc_attr( get_the_title() ) . '"  target="_blank" onclick="__gaTracker(\'send\', \'event\', \'outbound-buytheme-affiliate\', \'' .   trailingslashit( get_field('purchase_url') ) . '\', \'' . esc_attr( get_the_title() ) . '\');" href="' . trailingslashit( get_field('purchase_url') ) . '" rel="nofollow">Buy now</a>';
			
		}
		
	}


	
	
}














genesis();
