<?php

// Force full width
add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );


//* Remove breadcrumbs
remove_action( 'genesis_before_loop', 'genesis_do_breadcrumbs' );


//* Enqueue and initialize jQuery Masonry script
add_action( 'wp_enqueue_scripts', 'phut_masonry_init' );
function phut_masonry_init() {


	// wp_enqueue_script( 'masonry-init', plugins_url() . '/powerhut-themes/assets/js/masonry-init.js' , array( 'masonry','jquery' ), PHUT_THEMES_VERSION, true );
	
	wp_enqueue_script( 'imagesLoaded', plugins_url() . '/powerhut-themes/assets/js/imagesloaded.pkgd.min.js', array('jquery'), PHUT_THEMES_VERSION, true );
	wp_enqueue_script( 'isotope', plugins_url() . '/powerhut-themes/assets/js/jquery.isotope.min.js' , array( 'imagesLoaded' ), PHUT_THEMES_VERSION, true );
	wp_enqueue_script( 'isotope-init', plugins_url() . '/powerhut-themes/assets/js/isotope-init.js' , array( 'isotope' ), PHUT_THEMES_VERSION, true );
	
 }



// Set Schema ?


// Remove the post info function from the entry header
remove_action ('genesis_meta','child_maybe_move_post_info' );
remove_action( 'genesis_entry_header', 'genesis_post_info', 12 );


remove_action( 'genesis_entry_content', 'genesis_do_post_image', 8 );


// Wrap all in masonry grid
add_action('genesis_before_while','phut_open_grid');
function phut_open_grid(){
	echo '<div class="grid"><div class="column-sizer"></div><div class="gutter-sizer"></div>';	
}
add_action('genesis_after_endwhile','phut_close_grid',9);
function phut_close_grid(){
	echo '</div>';	
}


// Change sort order
add_action( 'genesis_before_loop', 'phut_do_query' );
/** Changes the Query before the Loop */
function phut_do_query() {
	global $query_string;
 	query_posts( wp_parse_args( $query_string, array(
		'orderby' => 'title',
		'order' => 'ASC',
		'posts_per_page' => -1
	) ) );
}

// Replace Genesis loop with custom
remove_action( 'genesis_loop', 'genesis_do_loop' );
add_action( 'genesis_loop', 'phut_theme_archive_loop' );


function phut_theme_archive_loop(){

	if ( have_posts() ) :

		do_action( 'genesis_before_while' );
		
		while ( have_posts() ) : the_post();

			do_action( 'genesis_before_entry' );
            phut_theme_summary();
            do_action( 'genesis_after_entry' );

		endwhile; //* end of all posts

		do_action( 'genesis_after_endwhile' );

	else : //* if no posts exist
		do_action( 'genesis_loop_else' );
	endif; //* end loop
	
} // phut_theme_archive_loop 



//* Add widget support for archive if widgets are being used
add_action( 'genesis_meta', 'phut_themes_archive_genesis_meta' );
function phut_themes_archive_genesis_meta() {

		// remove_action( 'genesis_before_loop', 'genesis_do_cpt_archive_title_description' );
		add_action( 'genesis_before_loop', 'phut_themes_filter', 12 );
		// add_action( 'genesis_before_loop', 'genesis_do_cpt_archive_title_description' , 15 );

} // fn


//* Add theme filter select boxes
function phut_themes_filter() { ?>
<div class="phut-themes-filter">
    
    <div class="filter-items">
    
        <div class="filter-item label"><span>Filter by:</span></div>

        <div class="filter-item">
            <select class="filter option-set" data-filter-group="supplier">
                <option selected="select" data-filter-value="">All Themes</option>
                <option data-filter-value=".phut_theme_category-powerhut">Powerhut</option>
                <option data-filter-value=".phut_theme_category-studiopress">StudioPress</option>
            </select>
        </div>

        <div class="filter-item">
            <select class="filter option-set" data-filter-group="layout">
                <option selected="select" data-filter-value="">All Layouts</option>
                <option data-filter-value=".phut_theme_tag-one-column">One Column</option>
                <option data-filter-value=".phut_theme_tag-two-columns">Two Columns</option>
                <option data-filter-value=".phut_theme_tag-three-columns">Three Columns</option>
           </select>
        </div>
         
    </div>

</div>

<?php } // fn



/* Add markup for homepage widgets
function phut_themes_archive_widget () {

	printf( '<div %s>', genesis_attr( 'phut-theme-archive-widgets' ) );
	genesis_structural_wrap( 'theme-archive' );

		genesis_widget_area( 'theme-archive-top-1', array(
			'before' => '<div class="phut-theme-archive-widgets-1 one-half first widget-area">',
			'after'	 => '</div>',
		) );

		genesis_widget_area( 'theme-archive-top-2', array(
			'before' => '<div class="phut-theme-archive-widgets-2 one-half widget-area">',
			'after'	 => '</div>',
		) );


	genesis_structural_wrap( 'theme-archive', 'close' );
	echo '</div>'; //* end .home-featured



	
	
	
} // fn


*/



genesis();
