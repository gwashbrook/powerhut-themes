<?php
/*
Plugin Name: Powerhut Themes
Description: Adds the phut_theme custom post type and associated taxonomies
Plugin URI: https://powerhut.net/
Author:      Graham Washbrook
Author URI:  https://powerhut.net/
Version:     0.0.2
*/

// namespace = phut_themes_

//* Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

define ('PHUT_THEMES_VERSION', '0.0.1');
define ('PHUT_THEMES_PATH', plugin_dir_path( __FILE__ ) );

// require( PHUT_THEMES_PATH . 'config.php');
require( PHUT_THEMES_PATH . 'inc/helpers.php');
require( PHUT_THEMES_PATH . 'inc/widgets/widgets.php');
require( PHUT_THEMES_PATH . 'inc/theme-taxonomies.php');
require( PHUT_THEMES_PATH . 'inc/theme-cpt.php');

/*
 * Use custom templates
 *
**/
add_filter('single_template', 'phut_themes_custom_templates');
add_filter('taxonomy_template',  'phut_themes_custom_templates');
add_filter('archive_template',  'phut_themes_custom_templates');

function phut_themes_custom_templates( $template ) {

	if ( is_singular('phut_theme') )
		return PHUT_THEMES_PATH . 'templates/single-phut_theme.php';
		
	if ( is_post_type_archive('phut_theme') || is_tax('phut_theme_category') || is_tax('phut_theme_tag') )
		return PHUT_THEMES_PATH . 'templates/archive-phut_theme.php';		




	return $template;

}


/*
 * Add image sizes
 *
 * 16:9
16	9
32	18
48	27
64	36
80	45
96	54
112	63
128	72
144	81
160	90
176	99
192	108
208	117
224	126
240	135
256	144
272	153
288	162
304	171
320	180
336	189
352	198 
368	207
384	216 * BOOM
400	225 
416	234 
432	243
448	252 
464	261
480	270
496	279
512	288
528	297
544	306
560	315
576	324
592	333
608	342
624	351
640	360
656	369
672	378
688	387
704	396
720	405
736	414
752	423
768	432
784	441
800	450
816 459 * BOOM
832 468
848 477

*/


add_image_size( 'theme-archive', 384, 216, array( 'left', 'top') ); // 16:9

/*
 * Init
 *
**/
add_action ('init', 'phut_themes_init' );

function phut_themes_init(){
	
	// Register the taxonomies
	phut_themes_register_theme_taxonomies();

	// Register the custom post type
	phut_themes_register_theme_cpt();

	// Better be safe than sorry when registering custom taxonomies for custom post types.  Use register_taxonomy_for_object_type()
	register_taxonomy_for_object_type( 'phut_theme_category', 'phut_theme' );
	register_taxonomy_for_object_type( 'phut_theme_tag', 'phut_theme' );

	// Filter the messages		
	add_filter( 'post_updated_messages', 'phut_themes_updated_msgs' );
	add_filter( 'bulk_post_updated_messages', 'phut_themes_bulk_updated_msgs' );
	
	// Filter the "Enter title here"
	add_filter( 'enter_title_here', 'phut_themes_enter_title' );		

} // phut_themes_init


/*
 * Filter post updated messages
 */
function phut_themes_updated_msgs( $messages ){

	global $post;

	$messages['phut_theme'] = array(
		0  => '', // Unused. Messages start at index 1.
		1  => sprintf( 'Theme updated. <a href="%s">View theme</a>', esc_url( get_permalink($post->ID) ) ),
		2  => 'Custom field updated.',
		3  => 'Custom field deleted.',
		4  => 'Theme updated.',
		5  => isset( $_GET['revision']) ? sprintf( 'Theme restored to revision from %s', wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6  => sprintf( 'Theme published. <a href="%s">View theme</a>', esc_url( get_permalink( $post->ID ) ) ),
		7  => 'Theme saved.',
		8  => sprintf( 'Theme submitted. <a target="_blank" href="%s">Preview theme</a>', esc_url( add_query_arg( 'preview', 'true', get_permalink( $post->ID ) ) ) ),
		9  => sprintf( 'Theme publishing scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview theme</a>', date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink( $post->ID ) ) ),
		10 => sprintf( 'Theme draft updated. <a target="_blank" href="%s">Preview theme</a>', esc_url( add_query_arg( 'preview', 'true', get_permalink( $post->ID ) ) ) ),
	);	



	return $messages;
	
}

function phut_themes_bulk_updated_msgs( $bulk_messages ){
	
	global $bulk_counts;
	
	$bulk_messages['phut_theme'] = array(
		'updated'   => _n( '%s theme updated.', '%s themes updated.', $bulk_counts['updated'] ),
		'locked'    => _n( '%s theme not updated, somebody is editing it.', '%s themes not updated, somebody is editing them.', $bulk_counts['locked'] ),
		'deleted'   => _n( '%s theme permanently deleted.', '%s themes permanently deleted.', $bulk_counts['deleted'] ),
		'trashed'   => _n( '%s theme deleted permanently.', '%s themes deleted permanently.', $bulk_counts['trashed'] ),	
		// 'untrashed' => _n( '%s theme restored from Trash.', '%s themes restored from the Trash.', $bulk_counts['untrashed'] ),
	);

	return $bulk_messages;
	
}


/*
 * Filter 'Enter Title Here' message
 */
function phut_themes_enter_title( $title ){
	$screen = get_current_screen();
	if( $screen->post_type == 'phut_theme' ) 
		$title = 'Enter a name for this theme';
	return $title;
}



// add_action( 'genesis_setup', 'phut_themes_sidebars' );
function phut_themes_sidebars() {

	//* Register widget areas
	genesis_register_sidebar( array(
		'id'          => 'theme-archive-top-1',
		'name'        => __( 'Theme Archive View 1', 'plugin' ),
		'description' => __( 'This is the theme archive top left section.', 'plugin' ),
	) );

	//* Register widget areas
	genesis_register_sidebar( array(
		'id'          => 'theme-archive-top-2',
		'name'        => __( 'Theme Archive View 2', 'plugin' ),
		'description' => __( 'This is the theme archive top right section.', 'plugin' ),
	) );


}



