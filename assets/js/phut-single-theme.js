// http://stackoverflow.com/questions/3514784/what-is-the-best-way-to-detect-a-mobile-device-in-jquery


jQuery(function( $ ){


	$(document).ready( function() {
		
		// Get hash value from URL
		var hash = window.location.hash;
		
		/*
		if( hash == '#demo-phone') {
			show_phone_demo();
		} else if ( hash == '#demo-tablet' ) {
			show_tablet_demo();
		} else if ( hash == '#demo-full' ) {
			show_full_demo();
		}
		*/
		if ( hash =='#demo-full' ) {
			show_full_demo();
		}


		$(".full-demo").click(function(e) {
			e.preventDefault();
			showFullDemo();
		});
		
		$(".close-demo").click( function(e){
			e.preventDefault();
			$("#theme-demo").fadeOut();
			
			$('#theme-demo .full-screen').fadeOut();
	
			
			// $.scrollTo('#single-theme');
			// $.scrollTo('-=50px');
			// $("body").css("overflow", "auto");
			// document.location.hash = "";
				
		});
		
		
		
	}); // doc ready


	function showDemo(){
		$("#theme-demo").show();
		$("body").css("overflow", "hidden");	
	}
	
	function showFullDemo(){
		showDemo();
		$("#theme-demo .full-screen").fadeIn();
		document.location.hash = "#demo-full";
	}
	
	

});