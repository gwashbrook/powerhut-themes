// JavaScript Document

jQuery(function($) {
	"use strict";
	var  $grid = $('.grid');
	$grid.imagesLoaded( function(){
		$grid.masonry({
			columnWidth: '.column-sizer',
			gutter: '.gutter-sizer',
			itemSelector: '.phut_theme',
			isAnimated: true,
			percentPosition: true,
			stagger: 30
		});
	});
});
