jQuery(document).ready( function($) {
   
	"use strict";

	var $grid = $('.grid'),
		$select = $( '.phut-themes-filter select' ),
		filters = {};
	
	
	// layout Isotope after each image loads
	$grid.imagesLoaded().progress( function() {
		
		$grid.isotope({
		  itemSelector: '.phut_theme',
		  percentPosition: true,
		  masonry: {
			columnWidth: '.column-sizer',
			gutter: '.gutter-sizer',
		  }
		});

	});


/*
// init Isotope
var $grid = $('.grid').isotope({
  // options...
});
// layout Isotope after each image loads
$grid.imagesLoaded().progress( function() {
  $grid.isotope('layout');
});
*/



	$select.change(function() {
		var $this = $(this);
		var group = $this.attr('data-filter-group');
		filters[group] = $this.find(':selected').attr('data-filter-value');
		var isoFilters = [];
		for( var prop in filters ) {
			isoFilters.push( filters[ prop ] );
		}
		var selector = isoFilters.join('');
		$grid.isotope({ filter: selector });	
		return false;
	}); // change


/*
	$(window).scroll (function(){
		
		if( $(document).scrollTop() > 80 ){
		
			$('.phut-themes-filter').addClass('fixed');	
			
		} else {
			$('.phut-themes-filter').removeClass('fixed');	
			
		}
		
	});
*/




});