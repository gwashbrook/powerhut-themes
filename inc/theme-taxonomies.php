<?php

// namespace = phut_themes_


/*
 * Register theme taxonomies
 *
 *
**/
function phut_themes_register_theme_taxonomies(){
	
	#1. Theme Category
	$labels = array (
		'name' => 'Theme Categories',
		'singular_name' => 'Theme Category',
		'menu_name' => 'Categories',
		'all_items' => 'All Categories',
		'edit_item' => 'Edit Category',		
		'view_item' => 'View Category',
		'update_item' => 'Update Category',
		'add_new_item' => 'Add New Theme Category',
		'new_item_name' => 'New Category',
		'parent_item' => 'Parent Category',
		'parent_item_colon' => 'Parent Category:',			
		'search_items' => 'Search Theme Categories',
		'popular_items' => 'Popular Categories',
		//'separate_items_with_commas' => 'Separate tags with commas',
		//'add_or_remove_items' => 'Add or remove categories',
		//'choose_from_most_used'  => 'Choose from the most used categories',	
	);
	
	// (boolean or array) (optional) Set to false to prevent automatic URL rewriting a.k.a. "pretty permalinks".
	// Pass an $args array to override default URL settings for permalinks. Default: true 
	$rewrite = array (
		'slug' => 'theme-category', // - Used as pretty permalink text (i.e. /tag/) - defaults to $taxonomy (taxonomy's name slug)
		'with_front' => false, // - allowing permalinks to be prepended with front base - defaults to true ??
		'hierarchical' => true // - true or false allow hierarchical urls (implemented in Version 3.1) 	
	);
	
	$capabilities = array(
		'manage_terms' => 'manage_theme_categories',
		'edit_terms' => 'edit_theme_categories',
		'delete_terms' => 'delete_theme_categories',
		'assign_terms' => 'assign_theme_categories',
	);	
	
	$taxonomy_args = array(
	
		'labels' => $labels,
		'public' => true,

		
		'show_ui' => true,            // defaults to value of public argument
		'show_in_nav_menus' => true,  // defaults to value of public argument
		// 'show_in_nav_menus' => false, // DON'T NEED THEM, SO HIDE
		
		'show_tagcloud' => false, //  Whether to allow the Tag Cloud widget to use this taxonomy, defaults to value of show_ui
		
		'show_in_quick_edit' => true, // defaults to value of show_ui
		
		
		// 'meta_box_cb'
		'show_admin_column' => true, // automatic creation of taxonomy columns on associated posts table
		
		
		
		'hierarchical' => true,
		// 'update_count_callback' => //  (string) (optional) A function name that will be called to update the count of an associated $object_type, such as post, is updated. Default: None 

		// 'query_var' => // (boolean or string) (optional) False to disable the query_var, set as string to use custom query_var instead of default which is $taxonomy, the taxonomy's "name". Default: $taxonomy
		// 'query_var' => true,
		'query_var' => false,
		
		/*
		Note:
		The query_var is used for direct queries through WP_Query like new WP_Query(array('people'=>$person_name))
		and URL queries like /?people=$person_name. Setting query_var to false will disable these methods, but you
		can still fetch posts with an explicit WP_Query taxonomy query like WP_Query(array('taxonomy'=>'people', 'term'=>$person_name)).
		*/
		
		
		'rewrite' => $rewrite, 
		'capabilities' => $capabilities,	
		// 'sort' => false, // default none // Whether this taxonomy should remember the order in which terms are added to objects
		
		// REST API
		
		'show_in_rest'       => false,
	    'rest_base'          => 'theme-category',
    /*
		'rest_controller_class' => 'WP_REST_Terms_Controller',
		*/




		// No archive so isotope filter on main theme archive makes more sense
		// 'public' => true,
		// 'show_in_nav_menus' => false,
		'show_tagcloud'     => false,
		'show_in_rest'       => false,





	);
	
	
	register_taxonomy( 'phut_theme_category', array( 'phut_theme' ), $taxonomy_args );


	#2. Theme Tag
	$labels = array(
		'name'          => 'Theme Tags',
		'singular_name' => 'Tag',
		'menu_name'     => 'Tags',
		'all_items'     => 'All Theme Tags',
		'edit_item'     => 'Edit Theme Tag',
		'view_item'     => 'View Tag',
		'update_item'   => 'Update Theme Tag',
		'add_new_item'  => 'Add New Theme Tag',
		'new_item_name' => 'New Tag',
		// 'parent_item'   => '', // not used on non hierarchical taxonomies
		// 'parent_item_colon'   => '', // not used on non hierarchical taxonomies. Same as parent_item, but with a colon :
		'search_items'  => 'Search Theme Tags',
		'popular_items' => 'Popular Tags',
		'separate_items_with_commas' => 'Separate tags with commas', // not used on hierarchical taxonomies
		'add_or_remove_items'        => 'Add or remove tags', // not used on hierarchical taxonomies
		'choose_from_most_used'      => 'Choose from the most used tags', // not used on hierarchical taxonomies
		'not_found'                  => 'No tags found', // not used on hierarchical taxonomies
	);
	
	$rewrite = array(
		'slug'         => 'theme-tag',  // Used as pretty permalink text (i.e. /tag/) - defaults to $taxonomy (taxonomy's name slug)
		'with_front'   => false,         // allowing permalinks to be prepended with front base - defaults to true
		// 'with_front'   => true,         // allowing permalinks to be prepended with front base - defaults to true
		'hierarchical' => false,         // Boolean. Allow hierarchical urls (implemented in Version 3.1) 	
	);
	
	$capabilities = array(
		'manage_terms' => 'manage_theme_tags',
		'edit_terms' => 'edit_theme_tags',
		'delete_terms' => 'delete_theme_tags',
		'assign_terms' => 'assign_theme_tags'
	);
	
	$args = array(
		'labels'            => $labels,
		// 'public'            => false, // optional
		'public'            => true, // optional
		'show_ui'           => true,
		// 'show_in_nav_menus' => false,
		'show_in_nav_menus' => true,
		'show_tagcloud'     => true, // defaults to show_ui
		// 'meta_box_cb'       => '', // Defaults to post_tags_meta_box() or post_categories_meta_box(). No metabox is shown if set to false
		'show_admin_column' => true,
		'hierarchical'      => false,
		// 'update_count_callback' => '', // Works like a hook
		'update_count_callback' => '_update_post_term_count',
		'query_var'         => false,
		'capabilities'      => $capabilities,
		'rewrite'           => $rewrite,
		
		/*
		'show_in_rest'       => true,
    	'rest_base'          => 'theme-tag',
		*/


		// No archive so isotope filter on main theme archive makes more sense
		'public' => true,
		'show_in_nav_menus' => false,
		'show_tagcloud'     => false,


		
		
	);
	
	register_taxonomy( 'phut_theme_tag', array( 'phut_theme' ), $args );


	
	// flush_rewrite_rules( false );
	

	
	
	
} //fn
