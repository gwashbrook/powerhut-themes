<?php

// namespace = phut_themes_

/*
 * Register theme custom post type
 */
function phut_themes_register_theme_cpt() {

	// The Theme Post Type
	$supports = array (
		'title',
		'excerpt',
		'editor',
		'author',
		'thumbnail',
		'custom-fields',
		// 'comments', // (also will see comment count balloon on edit screen)
		// 'revisions', // (will store revisions)
		// 'genesis-simple-menus',
		// 'genesis-seo',
		// 'genesis-layouts',
		// 'genesis-simple-sidebars',
		'genesis-scripts',
		'genesis-cpt-archives-settings',
		// 'autodescription-meta', // SEO framework
		// 'page-attributes', // menu order (hierarchical must be true)
		// 'genesis-entry-meta-after-content', ?
		// 'genesis-entry-meta-before-content', ?
		'genesis-adjacent-entry-nav',
	);

	$labels = array (
		'name'               => 'Themes',
		'singular_name'      => 'Theme',
		'menu_name'          => 'Themes',
		'name_admin_bar'     => 'Theme',
		'all_items'          => 'All Themes',
		'add_new'            => 'Add New',
	 	'add_new_item'       => 'Add New Theme',
		'edit_item'          => 'Edit Theme',
		'new_item'           => 'New Theme',
		'view_item'          => 'View Theme',
		'search_items'       => 'Search Themes',
		'not_found'          => 'No Themes Found',
		'not_found_in_trash' => 'No Themes found in Trash',
		// 'parent_item_colon'  => 'Parent Theme', // only used in hierarchical post types
	);
	
	$rewrite = array (
		'slug'          => 'themes',
		'with_front'    => false, // Should the permalink structure be prepended with the front base. (example: if your permalink structure is /blog/, then your links will be: false->/news/, true->/blog/news/). Defaults to true 
		'feeds'         => false, // Defaults to has_archive value
		'pages'         => true, // Should the permalink structure provide for pagination. Defaults to true
	);		
	
	$taxonomies = array (
		// 'phut_theme_category'
	);
	
	$args = array(
		'label'                => 'Themes',
		'labels'               => $labels,
		'description'          => '', // (string) (optional)

		'public'               => true, // true || false - optional : Needs to be true for Yoast
			'exclude_from_search'  => false,
			'publicly_queryable'   => true,
			'show_in_nav_menus'    => true,
			'show_ui'	           => true,
	
		'has_archive'          => true,
		'show_in_admin_bar'    => true,
		// 'menu_position'        => NULL, // Defauts to below Comments	
		'menu_icon'            => 'dashicons-desktop', // The url to the icon to be used for this menu or the name of the dashicon
		// 'show_in_menu'         => true, ???

		'capability_type'      => 'phut_theme', // referenced
		'map_meta_cap'         => true,
		'hierarchical'         => false,
		'supports'             => $supports,
		// 'register_meta_box_cb' => '', // Called when setting up the meta boxes for the edit form. The callback function takes one argument $post, which contains the WP_Post object for the currently edited post. Do remove_meta_box() and add_meta_box() calls in the callback.
		'taxonomies'          => $taxonomies,

		'rewrite'              => $rewrite,		
		'query_var'            => true, // defaults to true (custom post type name)
		'can_export'           => true, // defaut true
	
		

		/* REST API 		
		'show_in_rest'       => true,
		'rest_base'          => 'themes',
		'rest_controller_class' => 'WP_REST_Posts_Controller', // default controller
		*/
	);
		
	register_post_type( 'phut_theme', $args );	
	
	// !!!!! Comment or Remove the following flush rewite rules when not in development !!!!!
	// See http://wordpress.org/support/topic/plugin-multi-page-toolkit-major-bad-practice-causes-60-second-page-loads
	// And Andrew Nacin's comment (senior WP dev ) here : http://wpengineer.com/2044/custom-post-type-and-permalink/
	
	// flush_rewrite_rules();
	
}
