<?php
if( !defined( 'ABSPATH' ) ) exit;

add_action( 'widgets_init', function(){
	register_widget( 'PHUT_THEMES_Widget_Theme_Categories' );
});


/**
 * Theme categories widget class
 *
 */
class PHUT_THEMES_Widget_Theme_Categories extends WP_Widget {

	public function __construct() {
		$widget_ops = array( 'classname' => 'widget_theme_categories', 'description' => __( "A dropdown of theme categories." ) );
		parent::__construct('theme-categories', __('Theme Categories Dropdown'), $widget_ops);
	}

	public function widget( $args, $instance ) {

		/** This filter is documented in wp-includes/default-widgets.php */
		$title = apply_filters( 'widget_title', empty( $instance['title'] ) ? false : $instance['title'], $instance, $this->id_base );


		$c = ! empty( $instance['count'] ) ? '1' : '0';
		$h = ! empty( $instance['hierarchical'] ) ? '1' : '0';

		echo $args['before_widget'];
		if ( $title ) {
			echo $args['before_title'] . $title . $args['after_title'];
		}

		// $cat_args = array('orderby' => 'name', 'show_count' => $c, 'hierarchical' => $h);

		if ( is_tax( 'phut_theme_category' ) ) {
			$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
			$selected = $term->slug;
		} else {
			$selected = -1;
		}

		$cat_args = array (
			'show_option_none' => 'All Themes',
			'show_count'       => $c,
			'hierarchical'     => $h,
      		'hide_empty'       => 0,
      		'taxonomy'         => 'phut_theme_category',
      		'depth'            => 1,
      		'orderby'          => 'name',
			'name'             => 'theme-category-dropdown-widget',
			'value_field'      => 'slug',
			'selected'         => $selected,
		);

		wp_dropdown_categories( $cat_args  );
?>

<script type="text/javascript">
/* <![CDATA[ */
(function() {
var themecatdropdown = document.getElementById("theme-category-dropdown-widget");
function onThemeCatChange() {
	if ( themecatdropdown.options[themecatdropdown.selectedIndex].value != -1 ) {
		location.href = "<?php echo esc_url( home_url( '/' ) ); ?>theme-category/"+themecatdropdown.options[themecatdropdown.selectedIndex].value+"/";
	} else {
		location.href = "<?php echo esc_url( home_url( '/' ) ); ?>themes/";
	}
}
themecatdropdown.onchange = onThemeCatChange;
})();
/* ]]> */
</script>

<?php


		echo $args['after_widget'];
	}

	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['count'] = !empty($new_instance['count']) ? 1 : 0;
		$instance['hierarchical'] = !empty($new_instance['hierarchical']) ? 1 : 0;
		return $instance;
	}

	public function form( $instance ) {
		//Defaults
		$instance = wp_parse_args( (array) $instance, array( 'title' => '') );
		$title = esc_attr( $instance['title'] );
		$count = isset($instance['count']) ? (bool) $instance['count'] :false;
		$hierarchical = isset( $instance['hierarchical'] ) ? (bool) $instance['hierarchical'] : false;
		
?>
		<p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e( 'Title:' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></p>

		<p><input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('count'); ?>" name="<?php echo $this->get_field_name('count'); ?>"<?php checked( $count ); ?> />
		<label for="<?php echo $this->get_field_id('count'); ?>"><?php _e( 'Show post counts' ); ?></label><br />

		<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('hierarchical'); ?>" name="<?php echo $this->get_field_name('hierarchical'); ?>"<?php checked( $hierarchical ); ?> />
		<label for="<?php echo $this->get_field_id('hierarchical'); ?>"><?php _e( 'Show hierarchy' ); ?></label></p>
<?php
	}

}
